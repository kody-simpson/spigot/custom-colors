package me.kodysimpson.rgbcolors;

import org.bukkit.plugin.java.JavaPlugin;

public final class RGBColors extends JavaPlugin {

    private static RGBColors plugin;

    @Override
    public void onEnable() {
        // Plugin startup logic

        plugin = this;

        //Setup/Load Config
        getConfig().options().copyDefaults();
        saveDefaultConfig();
        reloadConfig();

        getCommand("color").setExecutor(new ColorCommand());

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public static RGBColors getPlugin() {
        return plugin;
    }
}
